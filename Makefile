reset: clean dev
release: clean build docker-unit clean docker-smoke push

clean:
	docker-compose stop || true
	docker-compose kill || true
	docker-compose rm -a -f|| true
	docker volume rm whatsapp-texty_database-data || true

build:
	docker-compose build textybot whatsapp-client textybot-site

dev:
	docker-compose up -d database
	sleep 4
	cd ./textybot/texty/persistent/migrations && alembic upgrade head

push:
	docker-compose build textybot whatsapp-client textybot-site
	docker-compose push textybot whatsapp-client textybot-site

push-to-server:
	ssh root@textybot.site "cd whatsapp-texty && make deploy"

up:
	docker-compose up textybot whatsapp-client database

deploy:
	# TODO: Make postgresql backup
	git pull
	docker-compose pull textybot whatsapp-client textybot-site
	docker-compose up -d textybot whatsapp-client textybot-site

docker-unit:
	docker-compose stop
	docker-compose build textybot
	docker-compose up -d database
	docker-compose -f docker-compose.yaml -f docker-compose-test-unit.yaml run textybot || exit 1
	docker-compose down


docker-smoke:
	docker-compose stop
	docker-compose build textybot textybot-site whatsapp-client
	docker-compose -f docker-compose.yaml -f docker-compose-test-smoke.yaml up -d database textybot whatsapp-client textybot-site
	sleep 2
	docker-compose -f docker-compose.yaml -f docker-compose-test-smoke.yaml run goss || exit 2
	docker-compose down

docker-tests: docker-unit docker-smoke

gitlab-test:
	gitlab-runner exec docker --docker-privileged texty

backupfile := file_$(shell date +%FT%T%Z).sql

backup-db-data:
	pg_dump -d texty -h localhost -p 9876 -U textybotread -W -f ~/Dropbox/Texty/backup/$(backupfile)

restore-db-data:
	psql -U textybot -h localhost -p 5432 -U texty -d texty -f ~/Dropbox/Texty/backup/$(backupfile)

import React from 'react';
import './App.css';
import {CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";

class Statistics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    componentDidMount() {
        fetch("https://api.whatsapp-texty.ru/statistics")
            .then(res => res.json())
            .then(result => result.map(
                value => {
                    const options = {month: 'long', day: 'numeric'};
                    return {
                        whatsapp: value.count,
                        date: new Date(value.timestamp * 1000).toLocaleDateString("ru-RU", options)
                    }
                }
            ))
            .then(
                (result) => {
                    this.setState({
                        data: result
                    });
                },
                // TODO: Обрабатывать исключения
                // (error) => {
                //     this.setState({
                //         isLoaded: true,
                //         error
                //     });
                // }
            )
    }

    render() {
        return (
            <div>
                <p className="pre-wrap lead">Сообщений перевел</p>
                <div style={{"marginLeft": -50}}>
                    <LineChart width={500} height={300} data={this.state.data}>
                        <XAxis dataKey="date" color="#fff"/>
                        <YAxis/>
                        <Line type="monotone" dataKey="whatsapp" stroke="#8BC34A"/>
                        <Tooltip/>
                        <CartesianGrid stroke="#eee"/>
                    </LineChart>
                </div>
            </div>
        );
    }
}

export default Statistics;

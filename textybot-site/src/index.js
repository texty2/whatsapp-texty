import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Languages from './Languages';

// ReactDOM.render(
//   <React.StrictMode>
//     <Statistics />
//   </React.StrictMode>,
//   document.getElementById('statistics')
// );
ReactDOM.render(
  <React.StrictMode>
    <Languages />
  </React.StrictMode>,
  document.getElementById('languages_')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

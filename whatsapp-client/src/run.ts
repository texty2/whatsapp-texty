import {Whatsapp} from 'venom-bot';
import {HTTPVenomWrapper} from "./rest";

import {WhatsappClient} from "./whatsapp";
import * as fs from "fs";
import {log, DOWNLOADS_FOLDER} from "./env";
import del = require("del");


async function clean_downloads() {
    if (fs.existsSync(DOWNLOADS_FOLDER)) {
        const deletedPaths = await del([`${DOWNLOADS_FOLDER}/*`], {force: true});
        console.log('Deleted files and directories:\n', deletedPaths.join('\n'));
    } else {
        fs.mkdirSync(DOWNLOADS_FOLDER)
        log.info(`Directory '${DOWNLOADS_FOLDER}' created from scratch`)
    }
}

clean_downloads().then((value => {
    log.info(`Directory '${DOWNLOADS_FOLDER}' was cleaned`)
})).catch(reason => {
    log.error(`Directory '${DOWNLOADS_FOLDER}' can't be cleaned`)
    log.error(reason)
    process.exit(1)
})

// Force exit if there is any rejection
process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', promise, 'reason:', reason);
    process.exit(10);
});

const wa = new WhatsappClient();
wa.whatsapp.then((whatsapp: Whatsapp) => {
    const server = new HTTPVenomWrapper(whatsapp);
    server.start();
})

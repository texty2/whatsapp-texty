console.log(process.env);

export const DOWNLOADS_FOLDER = process.env.DOWNLOADS_FOLDER || '../files/downloads';
export const REMOTE_DOWNLOADS_FOLDER = process.env.REMOTE_DOWNLOADS_FOLDER || DOWNLOADS_FOLDER;

export const HTTP_WEBHOOK_URL = process.env.HTTP_WEBHOOK_URL;
if (HTTP_WEBHOOK_URL == undefined) {
    console.error("Specify HTTP_WEBHOOK_URL");
    process.exit(1);
}
export const X_VENOM_TOKEN = process.env.X_VENOM_TOKEN;
if (X_VENOM_TOKEN == undefined) {
    console.error("Specify X_VENOM_TOKEN");
    process.exit(1);
}

export const CHROME_ARGS = process.env.CHROME_ARGS || ''
export const CHROME_HEADLESS = process.env.CHROME_HEADLESS == "true" || false

const LOG_LEVEL = process.env.LOG_LEVEL || 'debug'
export let log = require('console-log-level')({level: LOG_LEVEL})

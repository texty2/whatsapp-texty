import {Whatsapp} from "venom-bot";
import * as fs from "fs";
import {log, DOWNLOADS_FOLDER, X_VENOM_TOKEN} from "./env";
import {createServer, IncomingMessage, Server, ServerResponse} from 'http';


//
// Auth
//
function auth(request: IncomingMessage): boolean {
    // We look at X-VENOM-TOKEN header or x-venom-token argument

    const url = new URL(request.url, "http://localhost/");
    const token_args = url.searchParams.get("x-venom-token")

    const token_header = request.headers["x-venom-token"]
    return token_header == X_VENOM_TOKEN || token_args == X_VENOM_TOKEN;
}

//
// Handlers
//
interface SendMessageBody {
    to_number: string,
    type: string,
    message: string,
    reply_to: string,
}

async function sendMessage(response: ServerResponse, whatsapp: Whatsapp, body: SendMessageBody) {
    const msg = body;
    log.debug(`Process message ${msg}`)
    if (msg.reply_to == undefined) {
        await whatsapp.sendText(msg.to_number, msg.message);
    } else {
        await whatsapp.reply(msg.to_number, msg.message, msg.reply_to, []);
    }
    whatsapp.stopTyping(body.to_number)
    response.statusCode = 201;
    response.end("Message was sent");
}

async function status(response: ServerResponse, whatsapp: Whatsapp) {
    const isConnected = await whatsapp.isConnected();
    const batteryLevel = await whatsapp.getBatteryLevel();
    const txt = `WhatsApp status: ${isConnected}
Battery level: ${batteryLevel}
`
    const statusOk = isConnected && batteryLevel >= 70
    if (statusOk) {
        response.statusCode = 200;
        response.end(txt)
    } else {
        throw new HTTPNotFoundError(txt)
    }
}

async function screenshot(response: ServerResponse, whatsapp: Whatsapp) {
    const screenshot_path = `${DOWNLOADS_FOLDER}/screenshot.png`
    await whatsapp.page.screenshot({path: screenshot_path});
    fs.readFile(screenshot_path, function (error, content) {
        const contentType = 'image/png';
        if (error) {
            if (error.code == 'ENOENT') {
                throw new HTTPNotFoundError('File not found: ${screenshot_path}')
            } else {
                throw new Error(`Can't read file ${screenshot_path}`)
            }
        } else {
            response.writeHead(200, {'Content-Type': contentType});
            response.end(content, 'utf-8');
        }
    });
}

interface Endpoint {
    path: string,
    method: string,
    handler: any,
}

const ENDPOINTS: Array<Endpoint> = [
    {method: "GET", path: "/screenshot", handler: screenshot},
    {method: "GET", path: "/status", handler: status},
    {method: "POST", path: "/sendMessage", handler: sendMessage},
]

//
// Server
//
class HTTPError extends Error {
    status: number
}

class HTTPUnauthorized extends HTTPError {
    status = 403
    message = "Token is empty or invalid"
}

class HTTPNotFoundError extends HTTPError {
    status = 404
}

class HTTPServerError extends HTTPError {
    status = 500
}

function error_to_response(error: any, response: ServerResponse) {
    if (error.status) {
        response.statusCode = error.status
        response.end(JSON.stringify({error: error.message, status: response.statusCode}))
    } else {
        log.error(error);
        response.statusCode = 500
        response.end(JSON.stringify({error: error.message, status: response.statusCode, stack: error.stack}))
    }
}

function catch_errors(func) {
    return async function (request: IncomingMessage, response: ServerResponse) {
        try {
            return await func(request, response)
        } catch (error) {
            error_to_response(error, response)
        }
    }
}

export class HTTPVenomWrapper {
    server: Server

    public constructor(public whatsapp: Whatsapp = null) {
        this.server = createServer(catch_errors(this.handle))
    }

    handle = (request: IncomingMessage, response: ServerResponse) => {
        // Prepare
        const url = new URL(request.url, "http://localhost/");
        log.debug(request.method, url.pathname)

        // Extract JSON and run the handler
        let data = ""
        const self = this;
        request.on('error', (err) => {
            console.error(err);
        }).on("data", function (chunk) {
            data += chunk;
        }).on("end", async function () {
            try {

                // Choose a handler
                const handlers = ENDPOINTS.filter((value => {
                    return value.method === request.method && value.path === url.pathname
                }))
                let handler
                if (handlers.length == 1) {
                    handler = handlers[0].handler
                } else if (handlers.length == 0) {
                    throw new HTTPNotFoundError()
                } else if (handlers.length >= 2) {
                    throw new HTTPServerError()
                }

                // Auth
                if (!auth(request)) {
                    throw new HTTPUnauthorized()
                }

                log.debug(`Process data ${data}`)
                let body = {}
                if (data !== "") {
                    body = JSON.parse(data);
                }

                await handler(response, self.whatsapp, body)
            } catch (error) {
                error_to_response(error, response)
            }
            log.info(request.method, url.pathname, response.statusCode)
        });
        return
    }

    public start() {
        const port = 5000;
        const host = 'localhost';
        this.server.on('error', function (e) {
            // Handle your error here
            log.error(e);
        });
        this.server.listen({
            // Listen to all address
            // host: host,
            port: port,
            exclusive: true
        });
        log.info(`HTTP server started http://${host}:${port}`)
    }
}
import mime = require('mime-types');

var request = require('requestretry');
import {create, Message, Whatsapp} from "venom-bot";
import * as fs from "fs";
import {CHROME_ARGS, CHROME_HEADLESS, HTTP_WEBHOOK_URL, log, DOWNLOADS_FOLDER, REMOTE_DOWNLOADS_FOLDER} from "./env";
import * as path from "path";
import {Subject} from "rxjs";

const SECOND = 1000;
const MINUTE = 60 * SECOND

const FILE_DELAY_REMOVE = 2 * MINUTE;
const FILE_URI_PREFIX = "file://"

export class WhatsappClient {
    whatsapp: Promise<Whatsapp>
    whatsappInstance: Whatsapp
    messages: Subject<Message>

    public constructor() {
        const self = this;
        this.messages = new Subject<Message>();
        self.whatsapp = create('sessionName', (base64Qr, asciiQR) => {
                log.info(asciiQR);
            },
            (statusFind) => {
                log.info(statusFind);
            }, {
                headless: CHROME_HEADLESS,
                devtools: false,
                useChrome: true,
                debug: false,
                logQR: true,
                browserArgs: [CHROME_ARGS],
                autoClose: 60000,
                disableSpins: true
            }
        ).then((client) => {
            self.start(client);
            return client;
        });
    }

    private start(client) {
        log.info('WhatsApp client creating...')
        // this.messages.subscribe(this.webhook)
        // client.onMessage(this.messages.next)
        client.onMessage((msg) => this.webhook(client, msg))
        log.info('WhatsApp client created!')
        this.whatsappInstance = client;
    }

    public async webhook(wa: Whatsapp, message: Message) {
        log.info(`Message ${message.id} from ${message.chatId} processing...`)
        log.debug(JSON.stringify(message))
        wa.sendSeen(message.chatId);

        // TODO: Do it sync
        if (message.isMMS || message.isMedia) {
            wa.startTyping(message.chatId)
            downloadAndDecryptMedia(wa, message).then(callWebhook);
        } else {
            callWebhook(message);
        }
    }
}

function downloadAndDecryptMedia(wa: Whatsapp, message: Message) {
    return wa.decryptFile(message).then((buffer) => {
        if (! message.mimetype.startsWith("audio")) {
            log.info(`The message ${message.id} has ${message.mimetype} media, skip it.`);
            return message
        }

        log.info(`The message ${message.id} has audio media, downloading it...`);
        const fileName = `${message.id}.${mime.extension(message.mimetype)}`;
        const filePath = path.resolve(`${DOWNLOADS_FOLDER}/${fileName}`)
        // TODO: Rewrite to async write
        log.info(`Writing file to ${filePath}...`)
        fs.writeFileSync(filePath, buffer);
        log.info(`The file from ${message.id} has been saved to ${filePath}`);
        const remoteFilePath = `${REMOTE_DOWNLOADS_FOLDER}/${fileName}`

        message.clientUrl = FILE_URI_PREFIX + path.resolve(remoteFilePath)
        // @ts-ignore
        message.filePath = FILE_URI_PREFIX + filePath
        return message
    });
}

function callWebhook(message) {
    log.debug(`Sending POST to ${HTTP_WEBHOOK_URL}...`)
    removeFile(message)
    request.post(
        HTTP_WEBHOOK_URL,
        {
            json: message,
            maxAttempts: 3,
            retryDelay: 15 * SECOND,
            retryStrategy: request.RetryStrategies.HTTPOrNetworkError
        },
        (error, res, body) => {
            if (error) {
                log.error(`Message ${message.id} from ${message.chatId} failed to reach webhook`)
                log.error(error)
                return
            }
            log.info(`Message ${message.id} from ${message.chatId} was processed`)
            log.info(`statusCode for ${message.id}: ${res.statusCode}`)
            log.debug(`Response: ${body}`)
        })
}

function removeFile(message) {
    if (message.filePath == undefined) {
        return
    }
    if (!message.filePath.startsWith(FILE_URI_PREFIX)) {
        return
    }

    const file = message.clientUrl.replace(FILE_URI_PREFIX, "")
    setTimeout(() => fs.unlink(file, () => {
        log.info(`File ${file} was removed`)
    }), FILE_DELAY_REMOVE)
}

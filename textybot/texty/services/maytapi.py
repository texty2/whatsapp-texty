import asyncio
import dataclasses
from typing import Dict
from typing import Mapping

import aiohttp
from texty import env
from texty.helpers.logs import logger
from texty.models.messenger import Incoming
from texty.models.messenger import Media
from texty.models.messenger import Message
from texty.models.messenger import Messenger
from texty.models.messenger import MessengerType
from texty.texts import Emoji


class MaytApi(Messenger):
    base = "https://api.maytapi.com/api"
    product_url = f"{base}/223ca1b8-38c7-44ab-a4ee-f54899340fec"

    def __init__(self, token: str):
        assert token, "Use token for MAYTAPI"
        assert env.MAYTAPI_PHONE_ID, "Set MAYTAPI_ADMIN_PHONE"

        headers = {"x-maytapi-key": token, "Content-Type": "application/json"}
        self._session = aiohttp.ClientSession(headers=headers)
        self._phone_id = env.MAYTAPI_PHONE_ID

    def type(self, msg: Incoming) -> MessengerType:
        return MessengerType.whatsapp

    async def send(self, msg: Message, commands: Dict = {}):
        helps = []
        for cmd, help in commands.items():
            emoji = Emoji[cmd]
            helps.append(f"- {emoji} */{cmd}*: {help}")
        if helps:
            msg.message += "\n" + "\n".join(helps)

        # https://api.maytapi.com/api/223ca1b8-38c7-44ab-a4ee-f54899340fec/2581/sendMessage
        url = f"{self.product_url}/{self._phone_id}/sendMessage"
        result = await self._session.post(url, json=dataclasses.asdict(msg))
        result.raise_for_status()

    def to_incoming(self, data: Mapping) -> Incoming:
        if "-" in data["conversation"]:
            # Group chat
            channel = data["conversation"]
        else:
            # User chat
            channel = data["user"]["id"]

        media: str = data["message"].get("url", "")
        mime: str = data["message"].get("mime", "")
        media_type = data["message"].get("type", "")

        # Filter only voice voice
        if media_type == "audio" or mime.startswith("audio/"):
            suffix = media.rsplit(".")[-1]
            url = media
            voice = Media(url, suffix)
        else:
            voice = None

        return Incoming(
            id=data["message"]["id"],
            channel=channel,
            text=data["message"].get("text"),
            voice=voice,
        )

    async def healhty(self) -> bool:
        # Collect statuses
        url = f"{self.product_url}/listPhones"
        result = await self._session.get(url)
        result.raise_for_status()
        data = await result.json()
        ids = [x["id"] for x in data]
        statuses = await asyncio.gather(*[self._phone_status(x) for x in ids])
        return all(statuses)

    async def _phone_status(self, phone_id: str) -> bool:
        log = logger.bind(phone_id=phone_id)
        url = f"{self.product_url}/{phone_id}/status"
        result = await self._session.get(url)
        result.raise_for_status()
        data = await result.json()
        success = data["success"]
        state = data["status"]["state"]["state"]
        log.debug(f"Phone status data: {data}")
        log.info(f"Phone state: {state}")
        return success and state == "CONNECTED"

import dataclasses
from typing import Dict
from typing import Mapping
from typing import Optional

import aiohttp
from texty import env
from texty.models.messenger import Incoming
from texty.models.messenger import Media
from texty.models.messenger import Message
from texty.models.messenger import Messenger
from texty.models.messenger import MessengerType
from texty.texts import Emoji


class VenomAPI(Messenger):
    base = env.VENOM_URL

    def __init__(self, token: str):
        assert token, "Use token for VENOM"
        headers = {"x-venom-token": token, "Content-Type": "application/json"}
        self._session = aiohttp.ClientSession(headers=headers)

    def type(self, msg: Optional[Incoming]) -> MessengerType:
        if msg is None:
            return MessengerType.whatsapp

        if "-" in msg.channel:
            return MessengerType.whatsapp_group

        return MessengerType.whatsapp

    async def send(self, msg: Message, commands: Dict = {}):
        helps = []
        for cmd, help in commands.items():
            emoji = Emoji[cmd]
            helps.append(f"- {emoji} */{cmd}*: {help}")
        if helps:
            msg.message += "\n" + "\n".join(helps)

        url = f"{self.base}/sendMessage"
        result = await self._session.post(url, json=dataclasses.asdict(msg))
        result.raise_for_status()

    def to_incoming(self, data: Mapping) -> Incoming:
        # Filter only voice voice
        if data["isMMS"] or data["isMedia"]:
            suffix = "ogg"
            url = data["clientUrl"]
            voice = Media(url, suffix)
        else:
            voice = None

        return Incoming(
            id=data["id"],
            channel=data["chat"]["id"],
            text=data.get("body"),
            voice=voice,
        )

    async def healhty(self) -> bool:
        url = f"{self.base}/status"
        result = await self._session.get(url)
        result.raise_for_status()
        return True

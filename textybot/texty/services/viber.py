import asyncio
import itertools
import sys
from typing import Dict

from texty.models.messenger import Incoming
from texty.models.messenger import Media
from texty.models.messenger import Message
from texty.models.messenger import Messenger
from texty.models.messenger import MessengerType
from viberbot import Api
from viberbot import BotConfiguration
from viberbot.api.messages import FileMessage
from viberbot.api.messages import RichMediaMessage
from viberbot.api.messages import TextMessage
from viberbot.api.viber_requests import ViberMessageRequest


class Viber(Messenger):
    def __init__(self, token):
        assert token
        bot_configuration = BotConfiguration(
            name="Texty",
            avatar="https://dl-media.viber.com/1/share/2/long/vibes/icon/image/0x0/9655/1dc84a005ad366dd23e648c7e1824389e50ff57fc48b5bba00c66f8782c19655.jpg",
            auth_token=token,
        )
        self.core = Api(bot_configuration)

    def type(self, msg: Incoming) -> MessengerType:
        return MessengerType.viber

    def register(self, url):
        self.core.set_webhook(url)

    async def send(self, msg: Message, commands: Dict[str, str] = {}) -> None:
        # TODO: Do it Async
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(None, lambda: self._sync_send(msg, commands))

    def to_incoming(self, data: ViberMessageRequest) -> Incoming:
        text = data.message.text if isinstance(data.message, TextMessage) else ""
        if isinstance(data.message, FileMessage):
            # TODO: Check it's sound
            suffix = data.message.file_name.rsplit(".")[-1]
            voice = Media(data.message.media, suffix)
        else:
            voice = None
        # TODO: Add id for replying
        return Incoming(id="", channel=data.sender.id, text=text, voice=voice)

    def _sync_send(self, msg: Message, commands: Dict):
        # TODO: Split direct\reply message
        messages = []
        if msg.message:
            messages.append(TextMessage(text=msg.message))

        if commands:
            # Use rich media https://developers.viber.com/docs/api/rest-bot-api/#rich-media-message--carousel-content-message
            cmd_buttons = list(
                itertools.chain.from_iterable(
                    [
                        [
                            {
                                "Columns": 6,
                                "Rows": 1,
                                "ActionType": "reply",
                                "ActionBody": f"/{cmd}",
                                "Text": f"<font color=#ffffff>/{cmd}</font>",
                                "BgColor": "#8074d6",
                                "TextSize": "large",
                                "TextVAlign": "middle",
                                "TextHAlign": "middle",
                            },
                            {
                                "Columns": 6,
                                "Rows": 2,
                                "ActionType": "none",
                                "Text": f"<font color=#000000>{help}</font>",
                            },
                        ]
                        for cmd, help in commands.items()
                    ]
                )
            )

            rich_media = {
                "Type": "rich_media",
                "ButtonsGroupColumns": 6,
                "ButtonsGroupRows": 6,
                "BgColor": "#FFFFFF",
                "Buttons": cmd_buttons,
            }
            # TODO: Use alt text correctly
            message = RichMediaMessage(
                rich_media=rich_media,
                alt_text="Your viber version is too old",
                min_api_version=2,
            )
            messages.append(message)
        self.core.send_messages(msg.to_number, messages)


if __name__ == "__main__":
    Viber(sys.argv[1]).register(sys.argv[2])

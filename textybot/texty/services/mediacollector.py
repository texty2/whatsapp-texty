import tempfile
from pathlib import Path

import aiofiles
from aiohttp import ClientSession
from texty.helpers.ffmpeg import ensure_mp3
from texty.helpers.ffmpeg import get_duration
from texty.helpers.files import background_remove
from texty.helpers.logs import logger
from texty.helpers.logs import timeit
from texty.models.messenger import Media


async def copy(source, dest):
    async with aiofiles.open(source, mode="rb") as s:
        async with aiofiles.open(dest, mode="wb") as f:
            await f.write(await s.read())


class MediaCollector:
    def __init__(self):
        self._session = ClientSession()

    @timeit("Collect voice files")
    async def collect_media(self, media: Media):
        if media is None:
            return

        file = tempfile.mktemp(suffix=f".{media.suffix}")

        if media.url.startswith("file://"):
            source = media.url.replace("file://", "")
            await copy(source, file)

        else:
            resp = await self._session.get(media.url)
            resp.raise_for_status()
            async with aiofiles.open(file, mode="wb") as f:
                await f.write(await resp.read())

        logger.debug(f"Collected {media.url} to {str(file)}")

        # Convert to MP3
        file_path = Path(file)
        mp3 = await ensure_mp3(file_path)
        await background_remove(file_path)
        duration = await get_duration(mp3)
        media.path = mp3
        media.duration = duration
        return media

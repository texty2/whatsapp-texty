from __future__ import annotations

import difflib
import random
from contextlib import suppress
from copy import deepcopy
from typing import Dict
from typing import Tuple

from texty.helpers.files import background_remove
from texty.helpers.logs import logger
from texty.models.channel import Channel
from texty.models.messenger import DirectMessage
from texty.models.messenger import Incoming
from texty.models.messenger import Messenger
from texty.models.messenger import MessengerType
from texty.models.messenger import ReplyMessage
from texty.models.stat import Stat
from texty.persistent.storage import EntityStorage
from texty.services.speech_to_text.base import SpeechToText
from texty.services.speech_to_text.google import LANGUAGE_CODE_TO_NAME
from texty.services.speech_to_text.google import LANGUAGE_NAME_TO_CODE
from texty.texts import Help
from texty.texts import Response

WIT_MAX_DURATION = 17


def get_new_year():
    symbols = [
        "\N{SNOWMAN}",
        "\N{SNOWFLAKE}",
        "\N{FATHER CHRISTMAS}",
        "\N{CHRISTMAS TREE}",
        "",
    ]
    return random.choice(symbols)


class Texty:
    def __init__(
        self,
        google: SpeechToText,
        wit: SpeechToText,
        storage: EntityStorage,
        messenger: Messenger,
    ):
        self.google = google
        self.wit = wit
        self.storage = storage
        self.messenger = messenger

    async def incoming(self, msg: Incoming):
        channel = await self.get_channel(msg=msg)
        if msg.voice:
            await self.speech_to_text(msg=msg, channel=channel)
        else:
            commander = Commander(storage=self.storage, channel=channel)
            response, commands = await commander.execute(msg.text)
            if response:
                out = DirectMessage(message=response, to_number=msg.channel)
                await self.messenger.send(msg=out, commands=commands)

        await self._send_news(msg=msg, channel=channel)

    async def get_channel(self, msg: Incoming) -> Channel:
        channel = await self.storage.get(Channel, Channel.hash(msg.channel))
        if channel is None:
            channel = Channel.create_new(
                sensitive_id=msg.channel,
                messenger=self.messenger.type(msg),
                language_code="ru-RU",
            )
            await self.storage.add(channel)
            commander = Commander(storage=self.storage, channel=channel)
            hello_text, _ = await commander.execute("/hello")
            hello_msg = DirectMessage(message=hello_text, to_number=msg.channel)
            await self.messenger.send(msg=hello_msg)

        if channel.messenger == MessengerType.unknown.name:
            channel.messenger = self.messenger.type(msg)
            await self.storage.update(channel)

        return channel

    async def speech_to_text(self, msg: Incoming, channel: Channel):
        text = await self._recognize(channel, msg)
        symbol = get_new_year()
        if symbol:
            text += f"\n{symbol}"

        # TODO: Remove even if have any error
        await background_remove(msg.voice.path)
        reply = ReplyMessage(to_number=msg.channel, reply_to=msg.id, message=text)
        await self.messenger.send(msg=reply)
        await self.storage.add(
            Stat(
                channel.id,
                self.messenger.type(msg),
                duration=msg.voice.duration,
            )
        )

    async def _recognize(self, channel, msg):
        assert msg.voice.duration is not None, "We can find duration!!!"
        if 0 < msg.voice.duration < WIT_MAX_DURATION:
            engines = [self.wit, self.google]
        else:
            engines = [self.google, self.wit]

        for engine in engines:
            try:
                text = await engine.recognize(msg.voice.path, channel.language_code)
                if text:
                    break
            except Exception:
                logger.exception(
                    f"We didn't manage to recognize text with {engine.__class__.__name__}"
                )
        else:
            # TODO: Move to ru.py
            text = "Texty: Кажется я не смог разобрать что сказано :("
        return text

    async def healhty(self, to_number: str) -> bool:
        statistic = await self.storage.statistics(
            days=1, messenger=self.messenger.type(None)
        )
        today_count = statistic[-1]["count"] if statistic else 0
        msg = DirectMessage(to_number=to_number, message=str(today_count))
        await self.messenger.send(msg=msg)

        return await self.messenger.healhty()  # type: ignore

    async def _send_news(self, msg: Incoming, channel: Channel) -> None:
        news_list = await self.storage.get_news(channel)
        for news in news_list:
            await self.messenger.send(
                msg=DirectMessage(to_number=msg.channel, message=news.text)
            )
            await self.storage.delete(news)


class Commander:
    def __init__(self, storage: EntityStorage, channel: Channel):
        self.channel = deepcopy(channel)
        self.storage = storage

    async def execute(self, text: str) -> Tuple[str, Dict]:
        if text:
            parts = text.split(" ")
            command = parts[0].lower().strip("/")
            argument = " ".join(parts[1:])
            cmd_func = getattr(self, f"cmd_{command}", None)

            if cmd_func is None:
                return None, None

            result = await cmd_func(argument)
            if isinstance(result, str):
                return result, {}
            return result
        return None, None

    def _visible_commands(self):
        commands = []
        for method in dir(self):
            if method.startswith("cmd_"):
                commands.append(method[4:])

        with suppress(ValueError):
            commands.remove("unknown")
        return commands

    async def cmd_unknown(self, *args, **kwargs) -> str:
        return Response["unknown"]

    async def cmd_help(self, *args, **kwargs) -> Tuple[str, Dict]:
        commands = {cmd: Help[cmd] for cmd in self._visible_commands()}
        return Response["help"], commands

    async def cmd_hello(self, *args, **kwargs) -> str:
        return Response["hello"]

    async def cmd_delete(self, *args, **kwargs) -> str:
        await self.storage.delete(self.channel)
        await self.storage.anonymize_statistic(self.channel)
        self.channel = None
        return Response["delete"]

    async def cmd_info(self, *args, **kwargs) -> str:
        language = LANGUAGE_CODE_TO_NAME[self.channel.language_code]
        days = 30
        statistics = await self.storage.statistics(
            days=days, messenger=None, channel=self.channel
        )
        messages_count = sum(x["count"] for x in statistics)
        return Response["info"].format(
            language=language, messages_count=messages_count, days=days
        )

    @staticmethod
    def _language_commands(adict: Dict, code_key: bool):
        if code_key:
            return {f"language {code}": name for code, name in adict.items()}
        else:
            return {f"language {code}": name for name, code in adict.items()}

    async def cmd_language(self, alanguage: str) -> str:
        if not alanguage:
            return Response["language_emtpy"].format(
                language=LANGUAGE_CODE_TO_NAME[self.channel.language_code]
            )

        # TODO: Move it to guess language
        if alanguage in LANGUAGE_CODE_TO_NAME:
            language_code = alanguage
            language = LANGUAGE_CODE_TO_NAME[language_code]
        elif alanguage in LANGUAGE_NAME_TO_CODE:
            language = alanguage
            language_code = LANGUAGE_NAME_TO_CODE[language]
        else:
            # Try to guess
            one_of_languages = difflib.get_close_matches(
                alanguage, LANGUAGE_NAME_TO_CODE.keys(), n=5, cutoff=0.4
            )
            if len(one_of_languages) != 1:
                line_format = "*/language {code}* - {language}"
                languages = "\n".join(
                    [
                        line_format.format(code=LANGUAGE_NAME_TO_CODE[x], language=x)
                        for x in one_of_languages
                    ]
                )
                return Response["language_one_of"].format(languages=languages)

            language = one_of_languages[0]
            language_code = LANGUAGE_NAME_TO_CODE[language]

        # Set
        self.channel.language_code = language_code
        await self.storage.update(self.channel)
        return Response["language_changed"].format(
            language=language, language_code=language_code
        )

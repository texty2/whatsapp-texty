from pathlib import Path

import aiohttp
from texty.helpers.ffmpeg import remove_pauses
from texty.helpers.files import background_remove
from texty.helpers.logs import timeit
from texty.services.speech_to_text.base import RecognizeError
from texty.services.speech_to_text.base import SpeechToText


class WIT(SpeechToText):
    """https://wit.ai/"""

    url = "https://api.wit.ai/speech"

    def __init__(self, token):
        assert token, "Use token"
        headers = {"Authorization": "Bearer " + token}
        self._session = aiohttp.ClientSession(headers=headers)

    async def recognize(self, file: Path, language_code: str = "all") -> str:
        with_no_pause = await remove_pauses(file)
        text = await self._recognize(with_no_pause)
        await background_remove(with_no_pause)
        if not text:
            raise RecognizeError()
        return text.capitalize()

    @timeit("wit.ai: Recognize speech")
    async def _recognize(self, wav: Path) -> str:
        result = await self._session.post(
            url=self.url,
            data=wav.read_bytes(),
            headers={"Content-Type": f"audio/mpeg3"},
        )
        result.raise_for_status()
        text = await result.json()
        return text.get("text", "")

import abc
from pathlib import Path


class RecognizeError(Exception):
    pass


class SpeechToText(abc.ABC):
    @abc.abstractmethod
    async def recognize(self, file: Path, language_code: str) -> str:
        pass

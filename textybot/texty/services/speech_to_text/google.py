import asyncio
import os
from pathlib import Path

from google.api_core.exceptions import InvalidArgument
from google.cloud import speech_v1
from google.cloud.speech_v1p1beta1.gapic import enums
from texty.helpers.logs import timeit
from texty.services.speech_to_text.base import RecognizeError
from texty.services.speech_to_text.base import SpeechToText
from texty.texts import RU


class GoogleSpeechToText(SpeechToText):
    """https://cloud.google.com/speech-to-text/"""

    def __init__(self, auth_path):
        assert Path(auth_path).exists(), (
            f"File '{auth_path}' must exist with Google Service Account: "
            "https://cloud.google.com/docs/authentication/getting-started"
        )
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = str(auth_path)
        self.client = speech_v1.SpeechClient()

    @timeit("Google: Recognize speech")
    async def recognize(self, file: Path, language_code: str) -> str:
        if language_code not in LANGUAGE_CODE_TO_NAME:
            raise RecognizeError(f"Unknown language code: {language_code}")

        loop = asyncio.get_running_loop()
        result = await loop.run_in_executor(
            None, lambda: self.sync_recognize(file, language_code)
        )
        if not result:
            raise RecognizeError()

        return result

    def sync_recognize(self, file: Path, language_code: str) -> str:
        sample_rate_hertz = 48000
        encoding = enums.RecognitionConfig.AudioEncoding.MP3
        config = {
            "language_code": language_code,
            "sample_rate_hertz": sample_rate_hertz,
            "encoding": encoding,
        }
        with file.open("rb") as f:
            content = f.read()
        audio = {"content": content}

        result = ""
        try:
            response = self.client.recognize(config, audio)
            result = " ...".join(x.alternatives[0].transcript for x in response.results)
        except InvalidArgument as e:
            if "too long" in e.message:
                return RU["error_long_message"]
        return result


LANGUAGE_NAME_TO_CODE = {
    "Afrikaans (South Africa)": "af-ZA",
    "Amharic (Ethiopia)": "am-ET",
    "Armenian (Armenia)": "hy-AM",
    "Azerbaijani (Azerbaijan)": "az-AZ",
    "Indonesian (Indonesia)": "id-ID",
    "Malay (Malaysia)": "ms-MY",
    "Bengali (Bangladesh)": "bn-BD",
    "Bengali (India)": "bn-IN",
    "Catalan (Spain)": "ca-ES",
    "Czech (Czech Republic)": "cs-CZ",
    "Danish (Denmark)": "da-DK",
    "German (Germany)": "de-DE",
    "English (Australia)": "en-AU",
    "English (Canada)": "en-CA",
    "English (Ghana)": "en-GH",
    "English (United Kingdom)": "en-GB",
    "English (India)": "en-IN",
    "English (Ireland)": "en-IE",
    "English (Kenya)": "en-KE",
    "English (New Zealand)": "en-NZ",
    "English (Nigeria)": "en-NG",
    "English (Philippines)": "en-PH",
    "English (South Africa)": "en-ZA",
    "English (Tanzania)": "en-TZ",
    "English (United States)": "en-US",
    "Spanish (Argentina)": "es-AR",
    "Spanish (Bolivia)": "es-BO",
    "Spanish (Chile)": "es-CL",
    "Spanish (Colombia)": "es-CO",
    "Spanish (Costa Rica)": "es-CR",
    "Spanish (Ecuador)": "es-EC",
    "Spanish (El Salvador)": "es-SV",
    "Spanish (Spain)": "es-ES",
    "Spanish (United States)": "es-US",
    "Spanish (Guatemala)": "es-GT",
    "Spanish (Honduras)": "es-HN",
    "Spanish (Mexico)": "es-MX",
    "Spanish (Nicaragua)": "es-NI",
    "Spanish (Panama)": "es-PA",
    "Spanish (Paraguay)": "es-PY",
    "Spanish (Peru)": "es-PE",
    "Spanish (Puerto Rico)": "es-PR",
    "Spanish (Dominican Republic)": "es-DO",
    "Spanish (Uruguay)": "es-UY",
    "Spanish (Venezuela)": "es-VE",
    "Basque (Spain)": "eu-ES",
    "Filipino (Philippines)": "fil-PH",
    "French (Canada)": "fr-CA",
    "French (France)": "fr-FR",
    "Galician (Spain)": "gl-ES",
    "Georgian (Georgia)": "ka-GE",
    "Gujarati (India)": "gu-IN",
    "Croatian (Croatia)": "hr-HR",
    "Zulu (South Africa)": "zu-ZA",
    "Icelandic (Iceland)": "is-IS",
    "Italian (Italy)": "it-IT",
    "Javanese (Indonesia)": "jv-ID",
    "Kannada (India)": "kn-IN",
    "Khmer (Cambodia)": "km-KH",
    "Lao (Laos)": "lo-LA",
    "Latvian (Latvia)": "lv-LV",
    "Lithuanian (Lithuania)": "lt-LT",
    "Hungarian (Hungary)": "hu-HU",
    "Malayalam (India)": "ml-IN",
    "Marathi (India)": "mr-IN",
    "Dutch (Netherlands)": "nl-NL",
    "Nepali (Nepal)": "ne-NP",
    "Norwegian Bokmål (Norway)": "nb-NO",
    "Polish (Poland)": "pl-PL",
    "Portuguese (Brazil)": "pt-BR",
    "Portuguese (Portugal)": "pt-PT",
    "Romanian (Romania)": "ro-RO",
    "Sinhala (Sri Lanka)": "si-LK",
    "Slovak (Slovakia)": "sk-SK",
    "Slovenian (Slovenia)": "sl-SI",
    "Sundanese (Indonesia)": "su-ID",
    "Swahili (Tanzania)": "sw-TZ",
    "Swahili (Kenya)": "sw-KE",
    "Finnish (Finland)": "fi-FI",
    "Swedish (Sweden)": "sv-SE",
    "Tamil (India)": "ta-IN",
    "Tamil (Singapore)": "ta-SG",
    "Tamil (Sri Lanka)": "ta-LK",
    "Tamil (Malaysia)": "ta-MY",
    "Telugu (India)": "te-IN",
    "Vietnamese (Vietnam)": "vi-VN",
    "Turkish (Turkey)": "tr-TR",
    "Urdu (Pakistan)": "ur-PK",
    "Urdu (India)": "ur-IN",
    "Greek (Greece)": "el-GR",
    "Bulgarian (Bulgaria)": "bg-BG",
    "Russian (Russia)": "ru-RU",
    "Serbian (Serbia)": "sr-RS",
    "Ukrainian (Ukraine)": "uk-UA",
    "Hebrew (Israel)": "he-IL",
    "Arabic (Israel)": "ar-IL",
    "Arabic (Jordan)": "ar-JO",
    "Arabic (United Arab Emirates)": "ar-AE",
    "Arabic (Bahrain)": "ar-BH",
    "Arabic (Algeria)": "ar-DZ",
    "Arabic (Saudi Arabia)": "ar-SA",
    "Arabic (Iraq)": "ar-IQ",
    "Arabic (Kuwait)": "ar-KW",
    "Arabic (Morocco)": "ar-MA",
    "Arabic (Tunisia)": "ar-TN",
    "Arabic (Oman)": "ar-OM",
    "Arabic (State of Palestine)": "ar-PS",
    "Arabic (Qatar)": "ar-QA",
    "Arabic (Lebanon)": "ar-LB",
    "Arabic (Egypt)": "ar-EG",
    "Persian (Iran)": "fa-IR",
    "Hindi (India)": "hi-IN",
    "Thai (Thailand)": "th-TH",
    "Korean (South Korea)": "ko-KR",
    "Chinese, Mandarin (Traditional, Taiwan)": "cmn-Hant-TW",
    "Chinese, Cantonese (Traditional, Hong Kong)": "yue-Hant-HK",
    "Japanese (Japan)": "ja-JP",
    "Chinese, Mandarin (Simplified, Hong Kong)": "cmn-Hans-HK",
    "Chinese, Mandarin (Simplified, China)": "cmn-Hans-CN",
}
LANGUAGE_CODE_TO_NAME = {code: name for name, code in LANGUAGE_NAME_TO_CODE.items()}

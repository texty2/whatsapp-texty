import tempfile
from pathlib import Path

from texty.helpers.asyncio import run
from texty.helpers.logs import logger
from texty.helpers.logs import timeit


@timeit("Convert to wav")
async def ensure_wav(file: Path) -> Path:
    return await _ensure(file, ".wav")


@timeit("Convert to mp3")
async def ensure_mp3(file: Path) -> Path:
    return await _ensure(file, ".mp3")


async def _ensure(file: Path, ext: str):
    file = Path(file)
    if file.name.endswith(ext):
        return file
    out = Path(tempfile.mktemp(suffix=ext))

    logger.info(f"Converting file {file} to {out}...")
    cmd = f"ffmpeg -i {file} {out}"
    await run(cmd)
    logger.info(f"File converted {out}")
    return out


async def get_duration(file: Path) -> float:
    cmd = f'ffprobe -i {file} -show_entries format=duration -v quiet -of csv="p=0"'
    out = await run(cmd)
    return float(out)


async def remove_pauses(file: Path) -> Path:
    out = Path(tempfile.mktemp(suffix=file.suffix))
    cmd = f"sox {file} {out} silence -l 1 0.1 1% -1 1.0 1%"
    await run(cmd)
    return out

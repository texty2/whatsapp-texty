import time

import structlog
from structlog import configure
from structlog import get_logger
from structlog.contextvars import bind_contextvars
from structlog.contextvars import clear_contextvars
from structlog.contextvars import merge_contextvars
from structlog.contextvars import unbind_contextvars

configure(
    processors=[
        structlog.processors.TimeStamper("iso"),
        structlog.processors.StackInfoRenderer(),
        structlog.dev.set_exc_info,
        structlog.processors.format_exc_info,
        merge_contextvars,
        structlog.processors.KeyValueRenderer(),
    ]
)

bind = bind_contextvars
unbind = unbind_contextvars
clear = clear_contextvars
get = get_logger

logger = structlog.get_logger()


def timeit(name: str = None):
    def decorator(method):
        async def timed(*args, **kw):
            nonlocal name
            name = name or method.__name__
            ts = time.time()
            log = logger.bind(method=name)
            try:
                log.debug(f"Started")
                result = await method(*args, **kw)
                return result
            finally:
                te = time.time()
                log.debug("Finished", duration=f"{te - ts:.2f}")

        return timed

    return decorator

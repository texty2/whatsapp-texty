import asyncio
import logging


async def run(cmd) -> str:
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await proc.communicate()

    logging.debug(f"[{cmd!r} exited with {proc.returncode}]")
    if stdout:
        logging.debug(f"[stdout]\n{stdout.decode()}")
    if stderr and proc.returncode:
        logging.error(f"[stderr]\n{stderr.decode()}")
        raise Exception("Can not convert to ...")
    return stdout

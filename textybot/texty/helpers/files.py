import asyncio
from pathlib import Path

from aiofiles import os
from texty.helpers.logs import logger


async def background_remove(path: Path) -> None:
    def done(future: asyncio.Future):
        if future.exception() is not None:
            logger.error("File removed error", exc_info=future.exception())
        else:
            logger.debug("File removed", file=path)

    task = asyncio.create_task(os.remove(path))
    task.add_done_callback(done)

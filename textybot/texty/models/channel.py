from __future__ import annotations

import hashlib
from dataclasses import dataclass

from texty.models.messenger import MessengerType


@dataclass
class Channel:
    id: str
    language_code: str
    messenger: MessengerType

    @classmethod
    def create_new(
        cls,
        sensitive_id: str,
        messenger: MessengerType,
        language_code: str = "ru-RU",
    ) -> Channel:
        return cls(
            id=cls.hash(sensitive_id),
            language_code=language_code,
            messenger=messenger,
        )

    @staticmethod
    def hash(id: str) -> str:
        return hashlib.sha256(id.encode()).hexdigest()

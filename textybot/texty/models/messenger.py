from __future__ import annotations

import abc
import enum
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Any
from typing import Dict
from typing import Optional


@dataclass
class Message:
    message: str
    to_number: str


@dataclass
class DirectMessage(Message):
    type: str = "text"


@dataclass
class ReplyMessage(Message):
    reply_to: str
    type: str = "text"


@dataclass
class Media:
    url: str
    suffix: str
    duration: float = None
    path: Path = None


@dataclass
class Incoming:
    id: str
    text: str
    channel: str
    voice: Media = None


class Messenger(abc.ABC):
    @abc.abstractmethod
    async def send(self, msg: Message, commands: Dict[str, str] = {}) -> None:
        ...

    @abc.abstractmethod
    def to_incoming(self, data: Any) -> Incoming:
        ...

    @abc.abstractmethod
    def type(self, msg: Optional[Incoming]) -> MessengerType:
        ...

    async def healhty(self) -> bool:
        return True


class MessengerType(Enum):
    whatsapp = enum.auto()
    viber = enum.auto()
    unknown = enum.auto()
    whatsapp_group = enum.auto()

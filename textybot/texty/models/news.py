from __future__ import annotations

from dataclasses import dataclass
from uuid import UUID

from texty.texts import NewsText


@dataclass
class News:
    id: UUID
    channel_id: str
    key: str

    @property
    def text(self):
        return NewsText[self.key]

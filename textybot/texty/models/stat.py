from dataclasses import dataclass

from texty.models.messenger import MessengerType


@dataclass
class Stat:
    channel_id: str
    messenger: MessengerType
    duration: float

from __future__ import annotations

import asyncio

from aiohttp import web
from texty import env
from texty.helpers.logs import logger
from texty.persistent.storage import EntityStorage
from texty.persistent.storage import init_database
from texty.rest.endpoints import health
from texty.rest.endpoints import incoming_maytapi
from texty.rest.endpoints import incoming_venom
from texty.rest.endpoints import incoming_viber
from texty.rest.endpoints import ping
from texty.rest.endpoints import statistics
from texty.rest.middleware import AccessLogger
from texty.services.maytapi import MaytApi
from texty.services.mediacollector import MediaCollector
from texty.services.speech_to_text.google import GoogleSpeechToText
from texty.services.speech_to_text.wit import WIT
from texty.services.textybot import Texty
from texty.services.venom import VenomAPI
from texty.services.viber import Viber


async def create_app():
    app = web.Application()
    app.add_routes(
        [
            web.get("/ping", ping),
            web.get("/health/{messenger}", health),
            web.get("/statistics", statistics),
            web.post("/incoming/maytapi", incoming_maytapi),
            web.post("/incoming/viber", incoming_viber),
            web.post("/incoming/venom", incoming_venom),
        ]
    )
    logger.info("Prepare services...")
    # Dependencies
    wit = WIT(env.WIT_ACCESS_TOKEN)
    storage = EntityStorage()
    google = GoogleSpeechToText(env.GOOGLE_APPLICATION_CREDENTIALS)
    viber = Viber(env.VIBER_TOKEN)
    venom = VenomAPI(env.VENOM_TOKEN)
    app["google"] = google
    app["collector"] = MediaCollector()
    app["venom"] = venom
    app["storage"] = storage
    app["wit"] = wit
    app["viber"] = viber
    texty_viber = Texty(google=google, wit=wit, storage=storage, messenger=viber)
    texty_whatsapp = Texty(google=google, wit=wit, storage=storage, messenger=venom)
    app["texty_viber"] = texty_viber
    app["texty_whatsapp"] = texty_whatsapp
    logger.info("Services were created")
    return app


async def arun():
    logger.info("Starting Texty...")
    await init_database()
    app = await create_app()

    runner = web.AppRunner(app, access_log_class=AccessLogger)
    await runner.setup()
    site = web.TCPSite(runner, "0.0.0.0", 8080)
    await site.start()

    logger.info("HTTP started")


def run():
    loop = asyncio.new_event_loop()
    loop.create_task(arun())
    loop.run_forever()


if __name__ == "__main__":
    run()

from aiohttp.abc import AbstractAccessLogger
from texty.helpers.logs import logger


class AccessLogger(AbstractAccessLogger):
    def __init__(self, *args, **kwargs) -> None:
        self.logger = logger

    def log(self, request, response, time):
        self.logger.info(
            f"{request.remote} "
            f'"{request.method} {request.path} '
            f"done in {time:.2f}s: {response.status}"
        )

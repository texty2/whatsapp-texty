import asyncio
import uuid

from aiohttp import web
from texty import env
from texty.helpers import logs
from texty.helpers.logs import logger
from texty.helpers.logs import timeit
from texty.models.messenger import Incoming
from texty.persistent.storage import EntityStorage
from texty.services.mediacollector import MediaCollector
from texty.services.textybot import Texty
from viberbot.api.messages import TextMessage
from viberbot.api.viber_requests import ViberFailedRequest
from viberbot.api.viber_requests import ViberMessageRequest
from viberbot.api.viber_requests import ViberSubscribedRequest


@timeit("POST /incoming/maytapi")
async def incoming_maytapi(request: web.Request):
    logs.bind(requestId=str(uuid.uuid4()))
    data = await request.json()

    if data["type"] != "message":
        logger.info("Don't process this type")
        return web.HTTPOk()

    collector = request.app["collector"]
    maytapi = request.app["maytapi"]
    texty = request.app["texty_whatsapp"]

    msg = maytapi.to_incoming(data)
    logs.bind(channel=msg.channel)

    msg.voice = await collector.collect_media(msg.voice)
    await texty.incoming(msg=msg)
    return web.HTTPOk()


@timeit("POST /incoming/venom")
async def incoming_venom(request: web.Request):
    logs.bind(requestId=str(uuid.uuid4()))
    data = await request.json()

    collector = request.app["collector"]
    venom = request.app["venom"]
    texty = request.app["texty_whatsapp"]

    msg = venom.to_incoming(data)
    logs.bind(channel=msg.channel)

    msg.voice = await collector.collect_media(msg.voice)
    await texty.incoming(msg=msg)
    return web.HTTPOk()


@timeit("POST /incoming/viber")
async def incoming_viber(request: web.Request):
    collector = request.app["collector"]
    viber = request.app["viber"]
    texty = request.app["texty_viber"]

    content = await request.content.read()
    if not viber.core.verify_signature(
        content, request.headers.get("X-Viber-Content-Signature")
    ):
        return web.Response(status=403)
    viber_request = viber.core.parse_request(content)

    if isinstance(viber_request, ViberMessageRequest):
        msg = viber.to_incoming(viber_request)

        def done(future: asyncio.Future):
            if future.exception() is not None:
                logger.error(
                    "Viber message processed with error:", exc_info=future.exception()
                )
            else:
                logger.debug("Viber message has been processed successfully")

        task = asyncio.create_task(viber_background(msg, texty, collector))
        task.add_done_callback(done)
        return web.Response(status=200)

    elif isinstance(viber_request, ViberSubscribedRequest):
        # TODO: Send hello
        viber.core.send_messages(
            viber_request.user.id, [TextMessage(text="thanks for subscribing!")]
        )
    elif isinstance(viber_request, ViberFailedRequest):
        logger.error(
            "client failed receiving message. failure: {0}".format(viber_request)
        )

    return web.Response(status=200)


async def viber_background(msg: Incoming, texty: Texty, collector: MediaCollector):
    msg.voice = await collector.collect_media(msg.voice)
    await texty.incoming(msg)


async def ping(request: web.Request):
    return web.Response(text="OK")


async def health(request: web.Request):
    messenger = request.match_info["messenger"]
    phones = {
        "viber": env.VIBER_ADMIN_PHONE,
        "whatsapp": env.VENOM_ADMIN_PHONE,
    }
    to_number = phones[messenger]
    texty: Texty = request.app[f"texty_{messenger}"]
    if await texty.healhty(to_number=to_number):
        return web.Response(text="OK")

    return web.HTTPNotFound(
        text="Some phones are down. Look at https://console.maytapi.com/"
    )


@timeit("GET /statistics")
async def statistics(request: web.Request):
    storage: EntityStorage = request.app["storage"]
    stat = await storage.statistics(days=30, messenger=None)
    return web.json_response(stat)

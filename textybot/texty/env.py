import os

POSTGRESQL_HOST = os.getenv("POSTGRESQL_HOST", "localhost")
POSTGRESQL_DB = os.getenv("POSTGRESQL_DB", "texty")
POSTGRESQL_USER = os.getenv("POSTGRESQL_USER", "texty")
POSTGRESQL_PASS = os.getenv("POSTGRESQL_PASS", "texty")
POSTGRESQL_PORT = os.getenv("POSTGRESQL_PORT", "5432")

VOICE_FILES_DIR = os.getenv("VOICE_FILES_DIR", "/tmp")

# Speech to text configs
GOOGLE_APPLICATION_CREDENTIALS = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")
WIT_ACCESS_TOKEN = os.getenv("WIT_ACCESS_TOKEN")

# LEGACY - WhatsApp (maytapi)
MAYTAPI_TOKEN = os.getenv("MAYTAPI_TOKEN")
MAYTAPI_ADMIN_PHONE = os.getenv("MAYTAPI_ADMIN_PHONE")
MAYTAPI_PHONE_ID = os.getenv("MAYTAPI_PHONE_ID")

# Viber
VIBER_TOKEN = os.getenv("VIBER_TOKEN")
VIBER_ADMIN_PHONE = os.getenv("VIBER_ADMIN_PHONE")

# Venom
VENOM_URL = os.getenv("VENOM_URL", "http://localhost:5000")
VENOM_TOKEN = os.getenv("VENOM_TOKEN")
VENOM_ADMIN_PHONE = os.getenv("VENOM_ADMIN_PHONE")

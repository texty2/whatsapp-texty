"""Add news 07

Revision ID: 7df4436c7536
Revises: 99885f7c9bff
Create Date: 2020-07-02 13:19:56.725120

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import table

# revision identifiers, used by Alembic.

# revision identifiers, used by Alembic.
revision = "7df4436c7536"
down_revision = "99885f7c9bff"
branch_labels = None
depends_on = None


def upgrade():
    news = table(
        "news",
        sa.Column("channel_id", sa.String(), nullable=True),
        sa.Column("key", sa.String(length=20), nullable=False),
    )
    channels = table(
        "channels", sa.Column("id", sa.String(), primary_key=True, nullable=False)
    )
    channels_stmt = channels.select()

    bind = op.get_bind()
    channel_ids = bind.execute(channels_stmt)
    for channel in channel_ids:
        channel_id = channel["id"]
        news_stmt = news.insert().values(key="2020_07", channel_id=channel_id)
        bind.execute(news_stmt)


def downgrade():
    pass

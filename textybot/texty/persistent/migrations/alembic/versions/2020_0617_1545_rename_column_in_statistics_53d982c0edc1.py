"""Rename column in statistics

Revision ID: 53d982c0edc1
Revises: b9796145905a
Create Date: 2020-06-17 15:45:19.618588

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "53d982c0edc1"
down_revision = "b9796145905a"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        "statistics", "number", new_column_name="channel_id", existing_nullable=True
    )


def downgrade():
    op.alter_column(
        "statistics", "channel_id", new_column_name="number", existing_nullable=True
    )

"""Add news 2020_06

Revision ID: ca9514a912e0
Revises: 969c7bdc3371
Create Date: 2020-06-22 15:00:41.781122

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import table

# revision identifiers, used by Alembic.

revision = "ca9514a912e0"
down_revision = "969c7bdc3371"
branch_labels = None
depends_on = None


def upgrade():
    news = table(
        "news",
        sa.Column("channel_id", sa.String(), nullable=True),
        sa.Column("key", sa.String(length=20), nullable=False),
    )
    channels = table(
        "channels", sa.Column("id", sa.String(), primary_key=True, nullable=False)
    )
    channels_stmt = channels.select()

    bind = op.get_bind()
    channel_ids = bind.execute(channels_stmt)
    for channel in channel_ids:
        channel_id = channel["id"]
        news_stmt = news.insert().values(key="2020_06", channel_id=channel_id)
        bind.execute(news_stmt)


def downgrade():
    pass

"""Rename messages to statistics

Revision ID: b9796145905a
Revises: c62ee46f8b16
Create Date: 2020-06-17 15:37:36.315489

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "b9796145905a"
down_revision = "c62ee46f8b16"
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table("messages", "statistics")


def downgrade():
    op.rename_table("statistics", "messages")

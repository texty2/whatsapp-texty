"""Add channel

Revision ID: e37362438961
Revises: e563c46cb433
Create Date: 2020-06-08 15:34:50.384109

"""
from typing import Any

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from alembic import op

# revision identifiers, used by Alembic.
revision = "e37362438961"
down_revision = "e563c46cb433"
branch_labels = None
depends_on = None


class Created(sa.Column):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            **dict(
                name="created",
                type_=pg.TIMESTAMP(timezone=True),
                nullable=False,
                server_default=sa.func.now(),
            )
        )


def upgrade():
    op.create_table(
        "channels",
        Created(),
        sa.Column("id", sa.String(), primary_key=True, nullable=False),
        sa.Column("state", sa.String, nullable=False),
    )


def downgrade():
    op.drop_table("channels")

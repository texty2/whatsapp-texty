"""Add messages

Revision ID: a477df4106ea
Revises: 
Create Date: 2020-06-03 15:33:58.683419

"""
from typing import Any

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from alembic import op

# revision identifiers, used by Alembic.
revision = "a477df4106ea"
down_revision = None
branch_labels = None
depends_on = None


class ID(sa.Column):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            **dict(
                kwargs,
                name="id",
                type_=pg.UUID,
                primary_key=True,
                server_default=sa.text("uuid_generate_v4()"),
            )
        )


class Created(sa.Column):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            **dict(
                name="created",
                type_=pg.TIMESTAMP(timezone=True),
                nullable=False,
                server_default=sa.func.now(),
            )
        )


def upgrade():
    op.get_bind().execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
    op.create_table(
        "messages",
        Created(),
        sa.Column("text", sa.String(), nullable=False),
        sa.Column("number", sa.String(50), nullable=False),
    )

    op.create_table(
        "ad",
        Created(),
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("text", sa.String()),
        sa.Column("priority", sa.Numeric, server_default="0"),
        sa.Column("showed", sa.Numeric, server_default="0"),
    )


def downgrade():
    op.drop_table("messages")
    op.drop_table("ad")

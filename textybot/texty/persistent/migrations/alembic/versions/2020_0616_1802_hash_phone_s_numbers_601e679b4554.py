"""Hash phone's numbers

Revision ID: 601e679b4554
Revises: e37362438961
Create Date: 2020-06-16 18:02:50.228299

"""
import hashlib

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from alembic import op
from sqlalchemy import table

# revision identifiers, used by Alembic.

revision = "601e679b4554"
down_revision = "e37362438961"
branch_labels = None
depends_on = None


def hash_(id: str) -> str:
    return hashlib.sha256(id.encode()).hexdigest()


def upgrade():
    op.alter_column(
        "messages",
        "number",
        existing_type=sa.String(length=50),
        type_=sa.String,
        existing_nullable=False,
    )

    messages = table(
        "messages",
        sa.Column("created", pg.TIMESTAMP),
        sa.Column("text", sa.String()),
        sa.Column("number", sa.String),
    )

    channels = table(
        "channels",
        sa.Column("created", pg.TIMESTAMP),
        sa.Column("id", sa.String(), primary_key=True, nullable=False),
        sa.Column("state", sa.String, nullable=False),
    )

    bind = op.get_bind()

    for t, column in zip((channels, messages), ("id", "number")):
        select = t.select()
        rows = bind.execute(select)
        for row in rows:
            id = row[column]
            hash = hash_(id)
            new_id = {column: hash}
            update = t.update().where(getattr(t.c, column) == id).values(**new_id)
            bind.execute(update)


def downgrade():
    pass

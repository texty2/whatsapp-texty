"""Add sickness news September 2020

Revision ID: 264258791167
Revises: 7df4436c7536
Create Date: 2020-10-05 12:48:07.238386

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import table

# revision identifiers, used by Alembic.
revision = "264258791167"
down_revision = "7df4436c7536"
branch_labels = None
depends_on = None


def upgrade():
    news = table(
        "news",
        sa.Column("channel_id", sa.String(), nullable=True),
        sa.Column("key", sa.String(length=20), nullable=False),
    )
    channels = table(
        "channels", sa.Column("id", sa.String(), primary_key=True, nullable=False)
    )
    channels_stmt = channels.select()

    bind = op.get_bind()
    channel_ids = bind.execute(channels_stmt)
    for channel in channel_ids:
        channel_id = channel["id"]
        news_stmt = news.insert().values(key="2020_09", channel_id=channel_id)
        bind.execute(news_stmt)


def downgrade():
    pass

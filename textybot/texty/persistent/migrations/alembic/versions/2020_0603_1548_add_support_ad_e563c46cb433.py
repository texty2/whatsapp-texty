"""Add Support AD

Revision ID: e563c46cb433
Revises: a477df4106ea
Create Date: 2020-06-03 15:48:15.100869

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "e563c46cb433"
down_revision = "a477df4106ea"
branch_labels = None
depends_on = None
from sqlalchemy import String
from sqlalchemy import orm
from sqlalchemy.sql import column
from sqlalchemy.sql import table


def upgrade():
    account = table("ad", column("text", String))

    bind = op.get_bind()
    session = orm.Session(bind=bind)


def downgrade():
    pass

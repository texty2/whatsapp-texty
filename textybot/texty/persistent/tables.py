from typing import Any

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from sqlalchemy import ForeignKey
from texty.models.messenger import MessengerType


class ID(sa.Column):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            **dict(
                kwargs,
                name="id",
                type_=pg.UUID,
                primary_key=True,
                server_default=sa.text("uuid_generate_v4()"),
            )
        )


class Created(sa.Column):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            **dict(
                name="created",
                type_=pg.TIMESTAMP(timezone=True),
                nullable=False,
                server_default=sa.func.now(),
            )
        )


metadata = sa.MetaData()

statistics = sa.Table(
    "statistics",
    metadata,
    Created(),
    sa.Column("channel_id", sa.String, nullable=False),
    sa.Column("messenger", sa.Enum(MessengerType), nullable=False),
    sa.Column("duration", sa.FLOAT, nullable=False),
)

channels = sa.Table(
    "channels",
    metadata,
    Created(),
    sa.Column("id", sa.String(), primary_key=True, nullable=False),
    sa.Column("language_code", sa.String(20), nullable=False),
    sa.Column("messenger", sa.Enum(MessengerType), nullable=False),
)

news = sa.Table(
    "news",
    metadata,
    ID(),
    Created(),
    sa.Column("channel_id", sa.String(), ForeignKey("channels.id", ondelete="CASCADE")),
    sa.Column("key", sa.String(20), nullable=False),
)

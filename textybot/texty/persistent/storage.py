import dataclasses
import typing
from datetime import datetime
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Type

import sqlalchemy as sa
from asyncpgsa import pg
from pytz import utc
from sqlalchemy import func
from sqlalchemy import text
from texty import env
from texty.models.channel import Channel
from texty.models.messenger import MessengerType
from texty.models.news import News
from texty.models.stat import Stat
from texty.persistent import tables
from texty.persistent import tables as t


async def init_database(host=None, password=None):
    host = host or env.POSTGRESQL_HOST
    password = password or env.POSTGRESQL_PASS
    await pg.init(
        host=host,
        port=env.POSTGRESQL_PORT,
        database=env.POSTGRESQL_DB,
        user=env.POSTGRESQL_USER,
        password=password,
    )


async def close_pool():
    await pg.pool.close()


T = typing.TypeVar("T")


class EntityStorage:
    _tables = {Channel: tables.channels, Stat: tables.statistics, News: tables.news}

    def _entity_to_db(self, entity) -> Dict:
        return dataclasses.asdict(entity)

    def _db_to_entity(self, cls: Type[T], row) -> T:
        attrs = list(cls.__dataclass_fields__.keys())
        return cls(**{x: row[x] for x in attrs})

    async def add(self, entity: T) -> None:
        table = self._tables[entity.__class__]
        data = self._entity_to_db(entity)
        stmt = table.insert().values(**data)
        await pg.fetchrow(stmt)

    async def get(self, cls: Type[T], id: Any) -> Optional[T]:
        table = self._tables[cls]
        stmt = table.select(table.c.id == id)
        row = await pg.fetchrow(stmt)
        if not row:
            return
        return self._db_to_entity(cls, row)

    async def update(self, entity: T) -> None:
        table = self._tables[entity.__class__]
        data = self._entity_to_db(entity)
        data.pop("id", None)
        stmt = table.update(table.c.id == entity.id).values(**data)
        await pg.fetchrow(stmt)

    async def delete(self, entity: T) -> None:
        table = self._tables[entity.__class__]
        stmt = table.delete(table.c.id == entity.id)
        await pg.fetchrow(stmt)

    async def anonymize_statistic(self, channel: Channel):
        stmt = (
            t.statistics.update()
            .where(t.statistics.c.channel_id == channel.id)
            .values(channel_id="0")
        )
        await pg.fetchrow(stmt)

    async def statistics(
        self, days: int, messenger: Optional[MessengerType], channel: Channel = None
    ) -> List[Dict[datetime, int]]:
        stmt = sa.select(
            [
                func.count().label("count"),
                func.date_trunc("day", t.statistics.c.created).label("day"),
            ]
        ).where(func.date_part("day", t.statistics.c.created - func.NOW()) < days)

        if channel:
            stmt = stmt.where(t.statistics.c.channel_id == channel.id)
        if messenger:
            stmt = stmt.where(t.statistics.c.messenger == messenger)

        stmt = stmt.group_by(text("day")).order_by(text("2"))
        row = await pg.fetch(stmt)
        # Convert datetime to timestamp
        data = [
            dict(count=x["count"], timestamp=x["day"].replace(tzinfo=utc).timestamp())
            for x in map(dict, row)
        ]
        return data

    async def get_news(self, channel: Channel):
        cls = News
        table = self._tables[cls]
        stmt = table.select(table.c.channel_id == channel.id).order_by(table.c.created)
        rows = await pg.fetch(stmt)
        if not rows:
            return []
        return [self._db_to_entity(cls, row) for row in rows]

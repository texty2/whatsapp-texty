RU = {
    "error_long_message": """TextyBot: Слишком длинное сообщение, я пока понимаю только меньше минуты :(
    
Поддержите поддержку длинных сообщений в TextyBot: https://textybot.site/support-long-messages
    """,
    "command_help_hello": "Сказать привет еще раз",
    "command_emoji_hello": "\N{WAVING HAND SIGN}",
    "command_response_hello": f"""
Привет! \N{WAVING HAND SIGN}
Кажется мы еще не знакомы?
Я - Texty, бот для WhatsApp\Viber. \N{DESKTOP COMPUTER}
Перевожу голосовые сообщения в текст.

\N{BLACK RIGHT-POINTING TRIANGLE} Добавь меня в контакты (предпочитаю имя *Texty*)
1\N{COMBINING ENCLOSING KEYCAP} Отправляй мне голосовые сообщения и я буду присылать тебе текст
2\N{COMBINING ENCLOSING KEYCAP} Пересылай мне голосовые сообщения когда у тебя нет возможности прослушать их и я буду присылать тебе текст

Давай начнем - перешли голосове сообщение и я попробую написать тебе текст \N{WHITE SMILING FACE}

Набери:
\N{BLACK QUESTION MARK ORNAMENT} language - чтобы посмотреть текущий язык или переключиться на распознование другого языка
\N{BLACK QUESTION MARK ORNAMENT} help - чтобы увидеть мой список команд 

\N{MOVIE CAMERA} Как пользоваться в WhatsApp: https://l.textybot.site/video-whatsapp
\N{MOVIE CAMERA} Как пользоваться в Viber: https://youtu.be/y_teG7blKv4
\N{LINK SYMBOL} Вконтакте: https://vk.com/textybot
\N{LINK SYMBOL} Instagram: https://www.instagram.com/textybot/
\N{PERSON WITH FOLDED HANDS} Поддержать проект: https://textybot.site/support


https://textybot.site/
Если видишь какую-то неисправность - пожалуйста, напиши на почту support@textybot.site
""",
    "command_help_help": "Показать доступные команды",
    "command_emoji_help": "\N{WRENCH}",
    "command_response_help": "В первую очередь Texty расшифровывает голосовые сообщения в текст :) "
                             "Просто отправь или перешли ему голосовое сообщение!\n\n"
                             "Также Texty понимает следующие команды:",
    "command_response_unknown": "Не понимаю такую команду :(",
    "command_help_delete": "Удалить данные. Texty забудет что общался с вами",
    "command_emoji_delete": "\N{HAMMER}",
    "command_response_delete": "Настройки сброшены!",
    "command_help_info": "Показать информацию по каналу - язык, количество переведенных сообщений",
    "command_emoji_info": "\N{CHART WITH UPWARDS TREND}",
    "command_response_info": "\N{MEMO}Вы используете *{language}* язык\n"
                             "\N{CHART WITH UPWARDS TREND}📝Расшифровано *{messages_count}* сообщений за {days} дней",
    "command_help_language": "Настройки языка. Покажет текущий язык, доступные языки или поменяет распознаваемый язык",
    "command_emoji_language": "\N{EARTH GLOBE EUROPE-AFRICA}",
    "command_response_language_emtpy": "\N{EARTH GLOBE EUROPE-AFRICA}\n"
                                       "Ваш текущий язык: *{language}*.\n"
                                       "Вы можете поменять его на один из языков:\n"
                                       "- *language ru-RU* - русский язык\n"
                                       "- *language en-US* - английский язык\n"
                                       "- *language fr-FR* - французский язык\n"
                                       "- *language it-IT* - итальянский язык\n"
                                       "- *language ja-JP* - японский язык\n"
                                       "- *language de-DE* - немецкий язык\n"
                                       " Не нашли нужный язык? Мы поддерживаем больше: "
                                       "https://cloud.google.com/speech-to-text/docs/languages",
    "command_response_language_one_of": "Не можем определить какой язык вы хотите поставить :(\n"
                                        "Может один из них?\n"
                                        "{languages}",
    "command_response_language_changed": "Теперь Texty будет распознавать *{language}* - *{language_code}* "
                                         "в этом чате!\n",
    "news_test": "The news from test!",
    "news_2020_06": "\N{BELL}\n"
                    "*Обновление Texty - июль 2020*\n"
                    "- Теперь Texty понимает команды - введи */help* чтобы посмотреть полный список\n"
                    "- Теперь Texty может расшифровать несколько языков - набери */language* чтобы узнать какие и как установить распознавание\n",
    "news_2020_07": "\N{BELL}\n"
                    "*Texty теперь доступен и в Viber!*\n"
                    "Он не доступен по номеру, но доступен по отдельной ссылке.\n"
                    "\nЗайти на сайт https://textybot.site/#viber и нажми на QR-код если ты на телефоне или отсканируй его в Viber!",
    "news_2020_09": "\N{BELL}\N{FACE WITH THERMOMETER}"
                    "*Texty снова начал работать!*\n"
                    "TextyBot был недоступен в сентябре 2020 года. Единственный его разработчик заболел пневмонией и Texty заболел вместе с ним\n"
                    "Проблемы решили. Теперь вы можете снова присылать голосовые сообщения и Texty будет переводить их!\n"
                    "Здоровья всем и носите маски в общественных местах\N{FACE WITH MEDICAL MASK}\n"
                    "\n\n*Texty* - перевожу голосовые сообщения в текст в WhatsApp и Viber https://textybot.site/\n"
                    "Как Texty работает в WhatsApp: https://l.textybot.site/video-whatsapp",
    "news_2020_10_insta": "\N{BELL}"
                          "\N{CAMERA}А у *textybot* появился аккаунт в Instagram.\n"
                          "Будем постить туда смешные истории и картинки про голосовые сообщения\n"
                          "Подключайтесь!\n"
                          "https://www.instagram.com/textybot/",
}

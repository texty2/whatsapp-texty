from texty.texts.ru import RU


class KeyTranslator:
    def __init__(self, prefix):
        self.prefix = prefix

    def __getitem__(self, item):
        return RU[f"{self.prefix}_{item}"]


Response = KeyTranslator("command_response")
Help = KeyTranslator("command_help")
Emoji = KeyTranslator("command_emoji")
NewsText = KeyTranslator("news")

#!/usr/bin/env bash
pushd /textybot/texty/persistent/migrations
alembic upgrade head
popd
ls -la /secrets
python -m texty
from setuptools import find_packages
from setuptools import setup

setup_params = dict(
    name="Texty",
    version="0.1.0",
    description="Texty - Speech to text",
    author_email="support@textybot.site",
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    license="MTI",
    keywords=[],
    platforms=["Linux"],
    include_package_data=True,
    zip_safe=False,
)


def main():
    setup(**setup_params)


if "__main__" == __name__:
    main()

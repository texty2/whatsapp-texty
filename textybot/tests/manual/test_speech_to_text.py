from pathlib import Path

import pytest
from texty import env
from texty.helpers.ffmpeg import ensure_mp3
from texty.services.speech_to_text.google import GoogleSpeechToText
from texty.services.speech_to_text.wit import WIT

pytestmark = pytest.mark.asyncio


@pytest.mark.parametrize(
    "filename,words",
    [
        ("hello.oga", "Привет"),
        ("old_format.mpga", "рапорт"),
        ("viber2.m4a", "Привет"),
        ("with_empty.oga", "пока"),
        ("one_two_three.oga", "Раз два три"),
    ],
)
async def test_recognize_wit(data_dir, filename, words, load_secrets):
    api = WIT(env.WIT_ACCESS_TOKEN)
    file = Path(data_dir / filename)
    mp3 = await ensure_mp3(file)
    text = await api.recognize(mp3)
    assert words in text


@pytest.fixture()
def google_auth_file():
    return Path(__file__).parent.parent.parent / "secrets" / "google-auth.json"


@pytest.mark.parametrize(
    "filename,words",
    [
        ("hello.oga", "Привет"),
        ("hello-44100.oga", "Привет"),
        ("old_format.mpga", "рапорт"),
        ("with_empty.oga", "пока"),
        ("viber2.m4a", "Привет"),
        ("viber_long.m4a", "попробуем протестировать"),
    ],
)
async def test_recognize_google(data_dir, filename, words, google_auth_file):
    api = GoogleSpeechToText(google_auth_file)
    file = Path(data_dir / filename)
    text = await api.recognize(file, language_code="ru-RU")
    assert words in text

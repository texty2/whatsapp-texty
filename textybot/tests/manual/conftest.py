import shutil

import pytest


@pytest.fixture()
def data_dir(tmpdir, original_data):
    shutil.copytree(str(original_data), str(tmpdir / "data"))
    return tmpdir / "data"

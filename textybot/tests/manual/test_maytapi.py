import pytest
from texty import env
from texty.models.messenger import DirectMessage
from texty.services.maytapi import MaytApi
from texty.services.textybot import Commander
from texty.texts import NewsText

pytestmark = pytest.mark.asyncio


@pytest.fixture()
def to_number():
    return env.MAYTAPI_ADMIN_PHONE


@pytest.fixture()
def maytapi():
    return MaytApi(env.MAYTAPI_TOKEN)


async def test_send(to_number, maytapi):
    msg = DirectMessage(to_number=to_number, message=NewsText["2020_07"])
    await maytapi.send(msg)


async def test_send_emoji(to_number, maytapi):
    maytapi = MaytApi(env.MAYTAPI_TOKEN)
    msg = DirectMessage(
        to_number=to_number, message="From test with \N{HEAVY BLACK HEART}..."
    )
    await maytapi.send(msg)


async def test_send_command_help(to_number, maytapi):
    header, commands = await Commander(None, None).cmd_help()
    msg = DirectMessage(to_number=to_number, message=header)
    await maytapi.send(msg, commands)

import pytest
from texty import env
from texty.models.channel import Channel
from texty.models.messenger import DirectMessage
from texty.services.textybot import Commander
from texty.services.viber import Viber
from texty.texts import NewsText

pytestmark = pytest.mark.asyncio


@pytest.fixture()
def to_number():
    return env.VIBER_ADMIN_PHONE


@pytest.fixture()
def viber():
    return Viber(env.VIBER_TOKEN)


async def test_send(to_number, viber):
    msg = DirectMessage(to_number=to_number, message=NewsText["2020_06"])
    await viber.send(msg)


async def test_send_command_help(to_number, viber):
    header, commands = await Commander(None, None).cmd_help()
    msg = DirectMessage(to_number=to_number, message=header)
    await viber.send(msg, commands)


async def test_send_command_language(to_number, viber):
    channel = Channel(None, "ru-RU")
    header, commands = await Commander(None, channel=channel).cmd_language("")
    msg = DirectMessage(to_number=to_number, message=header)
    await viber.send(msg, commands)

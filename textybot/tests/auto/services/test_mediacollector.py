import pytest
from texty.models.messenger import Media
from texty.services.mediacollector import MediaCollector

pytestmark = pytest.mark.asyncio


async def test_mediacollector(original_data):
    url = f"file://{original_data}/viber_long.m4a"
    collector = MediaCollector()
    media = Media(url=url, suffix="m4a")
    new_media = await collector.collect_media(media)
    assert new_media.path
    assert new_media.path.suffix == ".mp3"
    assert new_media.duration == 44.82

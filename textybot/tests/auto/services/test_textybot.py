from pathlib import Path
from typing import Any
from uuid import uuid4

import pytest
from texty.models.channel import Channel
from texty.models.messenger import Incoming
from texty.models.messenger import Media
from texty.models.messenger import Message
from texty.models.messenger import Messenger
from texty.models.messenger import MessengerType
from texty.models.news import News
from texty.models.stat import Stat
from texty.services.speech_to_text.base import SpeechToText
from texty.services.textybot import Commander
from texty.services.textybot import Texty
from texty.services.venom import VenomAPI

pytestmark = pytest.mark.asyncio
fixture = pytest.lazy_fixture


class NamedSpeechToText(SpeechToText):
    def __init__(self, name: str):
        self.name = name

    async def recognize(self, file: Path, language_code) -> str:
        return f"{self.name}|{language_code}"


class MessengerWhatsappTest(Messenger):
    def __init__(self):
        self.messages = []

    async def send(self, msg: Message) -> None:
        self.messages.append(msg.message)

    def to_incoming(self, data: Any) -> Incoming:
        return data

    def type(self, msg: Incoming):
        return MessengerType.whatsapp


@pytest.fixture()
def messenger():
    return MessengerWhatsappTest()


@pytest.fixture()
def texty(storage, messenger):
    return Texty(
        google=NamedSpeechToText("google"),
        wit=NamedSpeechToText("wit"),
        storage=storage,
        messenger=messenger,
    )


@pytest.fixture()
def sensitive_channel_id():
    return "79111111111@c.us"


@pytest.fixture()
def channel(sensitive_channel_id):
    return Channel.create_new(
        sensitive_id=sensitive_channel_id,
        messenger=MessengerType.whatsapp,
        language_code="fr-FR",
    )


@pytest.fixture()
async def channel_known(storage, channel):
    await storage.add(channel)
    return channel


@pytest.fixture()
def text_incoming(sensitive_channel_id):
    return Incoming(
        id=str(uuid4()), text="Any text message", channel=sensitive_channel_id
    )


@pytest.fixture()
def voice_incoming_15s(sensitive_channel_id):
    return Incoming(
        id=str(uuid4()),
        text="Any text message",
        channel=sensitive_channel_id,
        voice=Media("http://any.url/path", ".suffix", duration=15.0),
    )


@pytest.fixture()
def voice_incoming_60s(sensitive_channel_id):
    return Incoming(
        id=str(uuid4()),
        text="Any text message",
        channel=sensitive_channel_id,
        voice=Media("http://any.url/path", ".suffix", duration=60.0),
    )


async def test_text(texty, text_incoming, messenger):
    await texty.incoming(msg=text_incoming)
    assert len(messenger.messages) == 1

    await texty.incoming(msg=text_incoming)
    assert len(messenger.messages) == 1, "Send hello message after first message"


@pytest.mark.parametrize(
    "incoming, text",
    [
        (fixture("voice_incoming_15s"), "wit|ru-RU"),
        (fixture("voice_incoming_60s"), "google|ru-RU"),
    ],
)
async def test_voice(texty, incoming, text, messenger):
    await texty.incoming(msg=incoming)
    assert len(messenger.messages) == 2, "Should send hello and voice"
    assert messenger.messages[-1] == text

    await texty.incoming(msg=incoming)
    assert len(messenger.messages) == 2 + 1, "Send hello message after first message"
    assert messenger.messages[-1] == text


@pytest.mark.parametrize(
    "message, expected",
    [
        (fixture("text_incoming"), ["The news from test!", "The news from test!"]),
        (
            fixture("voice_incoming_15s"),
            ["wit|fr-FR", "The news from test!", "The news from test!"],
        ),
    ],
)
async def test_news(
    texty, message: Incoming, expected, messenger, storage, channel_known: Channel
):
    # Create two news
    await storage.add(News(uuid4(), channel_id=channel_known.id, key="test"))
    await storage.add(News(uuid4(), channel_id=channel_known.id, key="test"))

    await texty.incoming(msg=message)

    assert messenger.messages == expected


@pytest.fixture()
async def channel_unknown_messenger(storage, sensitive_channel_id):
    channel = Channel.create_new(
        sensitive_id=sensitive_channel_id,
        messenger=MessengerType.unknown,
        language_code="fr-FR",
    )
    await storage.add(channel)
    return channel


class TestTextybot:
    async def test_update_channel_messenger_type_if_unknown(
        self, channel_unknown_messenger, text_incoming, texty, storage
    ):
        await texty.incoming(msg=text_incoming)
        channel = await storage.get(Channel, Channel.hash(text_incoming.channel))
        assert channel.messenger == MessengerType.whatsapp.name


@pytest.mark.parametrize(
    "command, is_command",
    [
        ("/help", True),
        ("help", True),
        ("Help", True),
        ("Help  ", True),
        ("/trololo asdf asfd", False),
    ],
)
async def test_commander(command, is_command, channel):
    commander = Commander(None, channel)
    text, cmds = await commander.execute(f"{command}")
    if is_command:
        assert isinstance(text, str)
    else:
        assert text is None


@pytest.fixture()
async def commander(storage, channel) -> Commander:
    await storage.add(channel)
    return Commander(storage, channel)


async def test_commander_delete(storage, channel):
    await storage.add(channel)
    commander = Commander(storage, channel)
    _ = await commander.execute(f"/delete")
    assert await storage.get(Channel, channel.id) is None


async def test_commander_info(storage, channel):
    await storage.add(channel)
    await storage.add(Stat(channel.id, MessengerType.whatsapp, duration=10.0))
    await storage.add(Stat(channel.id, MessengerType.whatsapp, duration=10.0))

    commander = Commander(storage, channel)

    info, _ = await commander.execute(f"/info")
    assert "French (France)" in info
    assert "*2*" in info

    # Check after reset channel
    await commander.execute(f"/delete")

    commander = Commander(storage, channel)
    info_deleteed, _ = await commander.execute(f"/info")
    assert "*0*" in info_deleteed


async def test_commander_language_emtpy(channel, commander: Commander, storage):
    empty, _ = await commander.execute(f"/language")
    assert "/language Russian" in empty


@pytest.mark.parametrize(
    "command, language, language_code",
    [
        ["en-US", "English (United States)", "en-US"],
        ["English (United States)", "English (United States)", "en-US"],
        # ["zulu", "Zulu (South Africa)", "zu-ZA"],
        ["Russian", "Russian (Russia)", "ru-RU"],
        ["Russia", "Russian (Russia)", "ru-RU"],
        ["Russain", "Russian (Russia)", "ru-RU"],
    ],
)
async def test_commander_language_set(
    command, language, language_code, channel, commander: Commander, storage
):
    changed, _ = await commander.execute(f"/language {command}")
    assert language in changed
    assert language_code in changed
    assert (await storage.get(Channel, channel.id)).language_code == language_code


async def test_commander_language_one_of(channel, commander: Commander, storage):
    command = "French"
    changed, _ = await commander.execute(f"/language {command}")
    assert "French (Canada)" in changed
    assert "fr-CA" in changed
    assert "French (France)" in changed
    assert "fr-FR" in changed

    assert (await storage.get(Channel, channel.id)).language_code == "fr-FR"

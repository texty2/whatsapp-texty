from uuid import uuid4

import pytest
import sqlalchemy as sa
from sqlalchemy import bindparam
from texty.models.channel import Channel
from texty.models.messenger import MessengerType


@pytest.mark.parametrize(
    "table_name, column, init_data",
    [("channels", "id", dict(state="new")), ("messages", "number", dict(text=""))],
)
def test_phone_is_hashed_after_601e679b4554(
    table_name, column, init_data, database_not_migrated, alembic_runner, alembic_engine
):
    metadata = sa.MetaData()
    messages = sa.Table(
        "messages",
        metadata,
        sa.Column("number", sa.String, nullable=False),
        sa.Column("text", sa.String, nullable=False),
    )
    channels = sa.Table(
        "channels",
        metadata,
        sa.Column("id", sa.String(), primary_key=True, nullable=False),
        sa.Column("state", sa.String, nullable=False),
    )

    table = locals()[table_name]
    phone = str(uuid4())
    phone_hash = Channel.hash(phone)

    # Fill with data
    alembic_runner.migrate_up_to("e37362438961")
    init_data[column] = phone
    alembic_engine.execute(table.insert().values(**init_data))

    # Migrate
    alembic_runner.migrate_up_to("601e679b4554")
    stmt = table.select(getattr(table.c, column) == bindparam("id")).limit(1)
    assert (
        len(list(alembic_engine.execute(stmt, id=phone))) == 0
    ), "ID must be hashed, but have found original one"
    assert (
        len(list(alembic_engine.execute(stmt, id=phone_hash))) == 1
    ), "ID must be hashed"


def test_statistic_default_is_whatsapp_after_ca9514a912e0(
    database_not_migrated, alembic_runner, alembic_engine
):
    stat_before = sa.Table(
        "statistics", sa.MetaData(), sa.Column("channel_id", sa.String, nullable=False)
    )
    stat_after = sa.Table(
        "statistics",
        sa.MetaData(),
        sa.Column("channel_id", sa.String, nullable=False),
        sa.Column("messenger", sa.Enum(MessengerType), nullable=False),
    )

    alembic_runner.migrate_up_to("ca9514a912e0")
    alembic_engine.execute(stat_before.insert().values(channel_id="channel.id"))

    # Migrate
    alembic_runner.migrate_up_to("99885f7c9bff")
    row = list(alembic_engine.execute(stat_after.select()))[0]
    assert row["messenger"] == MessengerType.whatsapp

from uuid import uuid4

import pytest
from texty.models.channel import Channel
from texty.models.messenger import MessengerType
from texty.models.news import News
from texty.models.stat import Stat
from texty.persistent.storage import EntityStorage

pytestmark = pytest.mark.asyncio


@pytest.fixture(autouse=True)
def setup(database_cleaned):
    pass


@pytest.fixture()
def uuid():
    return str(uuid4())


async def test_channel(uuid, storage: EntityStorage):
    # Get non-existing
    assert await storage.get(Channel, uuid) is None
    channel = Channel(id=uuid, messenger=MessengerType.whatsapp, language_code="ru-RU")

    # Create, get existing
    await storage.add(channel)
    assert isinstance(await storage.get(Channel, uuid), Channel)
    assert (await storage.get(Channel, uuid)).id == uuid

    # Update
    channel_old = Channel(
        id=uuid, messenger=MessengerType.whatsapp_group, language_code="en-US"
    )
    await storage.update(channel_old)
    assert (await storage.get(Channel, uuid)).language_code == "en-US"
    assert (await storage.get(Channel, uuid)).messenger == "whatsapp_group"

    # Delete
    await storage.delete(channel_old)
    assert await storage.get(Channel, uuid) is None


async def test_statistics(storage):
    stat0 = await storage.statistics(14, messenger=MessengerType.whatsapp)
    assert len(stat0) == 0

    await storage.add(
        Stat("channel.id", messenger=MessengerType.whatsapp, duration=1.0)
    )
    stat_whatsapp = await storage.statistics(14, messenger=MessengerType.whatsapp)
    assert len(stat_whatsapp) == 1

    stat_viber = await storage.statistics(14, messenger=MessengerType.viber)
    assert len(stat_viber) == 0, "We must differ messenger type"


@pytest.mark.parametrize("uuid", ["1", "1"])
async def test_clean_db_on_test(uuid, storage):
    # Get non-existing
    assert await storage.get(Channel, uuid) is None
    channel = Channel(id=uuid, messenger=MessengerType.whatsapp, language_code="en-US")

    # Create, get existing
    await storage.add(channel)


async def test_news(storage, uuid):
    channel = Channel(id=uuid, messenger=MessengerType.whatsapp, language_code="en-US")
    channel2 = Channel(
        id=str(uuid4()), messenger=MessengerType.whatsapp, language_code="en-US"
    )
    await storage.add(channel)
    await storage.add(channel2)
    news0 = await storage.get_news(channel)
    assert news0 == []
    await storage.add(News(id=uuid4(), channel_id=channel2.id, key="news_for_channel2"))
    await storage.add(News(id=uuid4(), channel_id=channel.id, key="news_for_test_0"))
    await storage.add(News(id=uuid4(), channel_id=channel.id, key="news_for_test_1"))

    news = await storage.get_news(channel)
    assert len(news) == 2
    assert [x.key for x in news] == ["news_for_test_0", "news_for_test_1"]

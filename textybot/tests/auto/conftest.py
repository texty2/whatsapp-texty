import contextlib
import os
from pathlib import Path
from time import sleep

import pytest
import sqlalchemy
from pytest_alembic.executor import CommandExecutor
from pytest_alembic.executor import ConnectionExecutor
from pytest_alembic.runner import MigrationContext
from pytest_docker.plugin import DockerComposeExecutor
from pytest_docker.plugin import Services
from texty.persistent.storage import close_pool
from texty.persistent.storage import EntityStorage
from texty.persistent.storage import init_database
from texty.persistent.tables import metadata


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig):
    return os.path.join(str(pytestconfig.rootdir.dirname), "docker-compose.yaml")


@contextlib.contextmanager
def get_docker_services(docker_compose_file, docker_compose_project_name, service):
    docker_compose = DockerComposeExecutor(
        docker_compose_file, docker_compose_project_name
    )

    # Spawn containers.
    docker_compose.execute(f"up -d {service}")

    # Let test(s) run.
    yield Services(docker_compose)

    # Clean up.
    docker_compose.execute("down -v")


@pytest.fixture(scope="session")
def database_host(docker_compose_file, docker_compose_project_name, postgresql_host):
    if postgresql_host:
        yield postgresql_host
        return

    with get_docker_services(
        docker_compose_file, docker_compose_project_name, service="database"
    ) as docker_service:
        # TODO: Check connections, don't sleep
        sleep(4)
        yield "localhost"


@contextlib.contextmanager
def runner(config, engine=None):
    """Manage the alembic execution context, in a given context.

    Yields:
        `MigrationContext` to the caller.
    """
    command_executor = CommandExecutor.from_config(config)
    migration_context = MigrationContext.from_config(
        config, command_executor, ConnectionExecutor(engine)
    )

    command_executor.configure(connection=engine)
    yield migration_context


@pytest.fixture(scope="session")
def alembic_dir():
    import texty.persistent.migrations

    dir = Path(texty.persistent.migrations.__file__).parent
    return dir


@pytest.fixture(scope="session")
def alembic_config(alembic_dir):
    return {
        "file": str(alembic_dir / "alembic.ini"),
        "script_location": str(alembic_dir / "alembic"),
    }


@pytest.fixture(scope="session")
def alembic_runner(alembic_config, alembic_engine):
    with runner(config=alembic_config, engine=alembic_engine) as r:
        yield r


@pytest.fixture(scope="session")
def alembic_engine(database_host):
    return sqlalchemy.create_engine(f"postgresql://texty@{database_host}:5432/texty")


@pytest.fixture()
async def database_not_migrated(alembic_runner, database_migrated):
    alembic_runner.migrate_down_to("a477df4106ea")
    alembic_runner.migrate_down_one()
    yield database_migrated
    alembic_runner.migrate_up_to("head")


@pytest.fixture(scope="session")
def database_migrated(alembic_runner, database_host):
    alembic_runner.migrate_up_to("head")
    return database_host


@pytest.fixture()
async def database_cleaned(alembic_engine, database_migrated):
    # TODO: Create template and clean DB on each test
    for table in metadata.tables.values():
        alembic_engine.execute(table.delete())
    yield database_migrated


@pytest.fixture()
async def storage(database_cleaned):
    await init_database(database_cleaned)
    yield EntityStorage()
    await close_pool()

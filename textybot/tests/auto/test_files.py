from asyncio import sleep
from pathlib import Path
from uuid import uuid4

import pytest
from texty.helpers.files import background_remove


@pytest.mark.asyncio
async def test_remove_later():
    path = Path(f"/tmp/{uuid4()}.tmp")
    path.touch()
    assert path.exists() is True
    await background_remove(path)
    await sleep(1)
    assert path.exists() is False

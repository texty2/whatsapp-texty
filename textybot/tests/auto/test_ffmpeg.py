import pytest
from texty.helpers.ffmpeg import ensure_mp3
from texty.helpers.ffmpeg import get_duration
from texty.helpers.ffmpeg import remove_pauses

pytestmark = pytest.mark.asyncio


async def test_ensure_mp3(original_data):
    oga = original_data / "hello.oga"
    mp3 = await ensure_mp3(oga)
    assert mp3.suffix == ".mp3"


async def test_get_duration(original_data):
    oga = original_data / "hello.oga"
    duration = await get_duration(oga)
    assert duration == 1.462167


async def test_remove_pauses(original_data):
    mp3_before = original_data / "with_empty.mp3"
    before = await get_duration(mp3_before)
    mp3_after = await remove_pauses(mp3_before)
    after = await get_duration(mp3_after)

    assert after < before

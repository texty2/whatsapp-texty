from pathlib import Path

import pytest
from dotenv import dotenv_values
from texty import env


def pytest_addoption(parser):
    parser.addoption("--postgres", action="store", default="", help="PostgreSQL HOST")


@pytest.fixture(scope="session")
def postgresql_host(request):
    host = request.config.getoption("postgres")
    return host


@pytest.fixture()
def original_data():
    return Path(__file__).parent / "data"


@pytest.fixture(autouse=True)
def load_secrets(monkeypatch):
    secrets_dir = Path(__file__).parent.parent.parent / "secrets"
    env_path = secrets_dir / "secrets.env"
    envs = dotenv_values(dotenv_path=env_path)
    for k, v in envs.items():
        # Ignore additional attributes
        if hasattr(env, k):
            monkeypatch.setattr(env, k, v)

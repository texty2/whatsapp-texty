# TextyBot
The project converts speech to text in WhatsApp, Viber and Telegram inside applications. You don't have to install any third application!

## Architecture
TextyBot is a system out of 3 components (we use names as their names in docker-compose.yaml file):
1. `database` - save channel's states, metrics 
2. `textybot` - it's a core engine. It converts audio files to appropriate formats, removes silents, sends to one of speech-to-text engine and sends back to Messenger. Works with any messenger that have an HTTP API.
3. `whatsapp-client` - just another yet client for WhatsApp, cause there's no one official client yet.

- `whatsapp-client` communicates to `textybot` via callback HTTP API.
- `textybot` sends commands to WhatsApp-client via HTTP API too.

Let's describe a little folder's structure:
- [textybot](./textybot) - the main project, core, our speech to text bot lives here.
- [whatsapp-client](./whatsapp-client) - WhatsApp API (Venom) - through this API we communicate with the WhatsApp's world.
- [textybot-site](./textybot-site) - web site for TextyBot [https://textybot.site/](https://textybot.site/)

Others folders:
- [files](./files) - temporary folders. Venom (WhatsApp API) saves files (audio and others) here
- [nginx](./nginx) - some nginx's files. In production environment we also place keys and tls certificates to this folder.
- [tests](./tests) - integration tests
- [web-redirect](./web-redirect) - old one for whatsapp-texty.com site. Remove it after `01 Jan 2021`

## Development
To create development environment (or play with TextyBot in your own instance) you must have a phone with installed `WhatsApp` or it's better to have 2 WhatsApp to be able to test communications.

Let's have a look how to run these components via docker and then we'll describe how to develop just one component and use others as a docker container.

### Secrets
TextyBot requires these files with secrets:
- `secrets/secrets.env` - you can copy and fill out from `.template` file
- `secrets/whatsapp-client.env` - you can copy and fill out from `.template` file
- `secrets/google-auth.json` - Google auth's file. Read more here https://cloud.google.com/docs/authentication/getting-started

### Docker
Install `docker`, allow your user work with `docker` cli

```
make clean dev
make dev build

#########
# Let's smoke run whatsapp
docker-compose up whatsapp-client
# Scan QR code in the first WhatsApp instance on a mobile phone

#########
# Let's smoke run textybot
docker-compose up textybot
# Verify there is no error in logs

#########
# Let's convert speech to text!
docker-compose up textybot whatsapp-client
# Send text from the second WhatsApp to the first and verify that you have "Hello" message
# Send a voice message less than 15 seconds (to check WIT.AI)
# Send a voice message longer than 20 seconds (to check Google API)
```


### Develop TextyBot
If you want to make a change in `textybot`, but want to use `whatsapp-client` as is, follow these steps:
- Find the right python's version in [Dockerfile](./textybot/Dockerfile)
- Install python, create venv, activate created virtualenv
- Install development's requirements:
```
cd textybot
pip install -m pipenv
pipenv install --dev
```
- Set env variable `export REMOTE_DOWNLOADS_FOLDER=/path/to/whatsapp-texty/files/downloads`
- Run whatsapp-client in terminal `docker-compose up whatsapp-client`, use QR code to authorize
- Set environment variables for `textybot` (look at `docker-compose.yaml` and `secrets.env`)
- Run `python -mtexty` in your favorite IDE!


### Develop whatsapp-client
If you want to make a change in `whatsapp-client`, but want to use `textybot` as is, follow these steps:
- Install `nodejs` (Find version in [Dockerfile](./whatsapp-client/Dockerfile))
- Run TextyBot in docker in one terminal (to look at logs sometimes) - `docker-compose up textybot`
- Install `whatsapp-client`. It's hard to describe it, have a look at [Dockerfile](./whatsapp-client/Dockerfile). In two words you must install `venom` from commit, install whatsapp-client and then create a link from venom.
- Configure environment variables, look at `docker-compose.yaml` service `whatsapp-client`
- Set env variable `REMOTE_DOWNLOADS_FOLDER=/app/files/downloads`
- Run `npm start` in your favorite IDE!

## Deploy to the production (by hand)
```
make release

# Deploy
ssh root@textybot.site "cd whatsapp-texty && make deploy"
```

## Deploy to the production (gitlab-ci)
In progress...

## Grafana
You can look at graphics with statistics

In the first terminal run:
```bash
ssh -L 9876:textybot.site:5432 root@textybot.site
```

In the second terminal run:
```bash
#
cd grafana
make up
# Set user/login to admin/admin
make restore

# Import dashboard from ./grafana/dashboards
```
User - `textybotread`